import pandas as pd
import numpy as np
from os import path
from matplotlib import pyplot as plt


class House:
    def __init__(self, input_path=None):
        # Set path
        defaul_path = path.join(path.dirname(__file__), "..", "data", "houses.csv")
        self.path = defaul_path if input_path is None else input_path
        # Load dateset and set dataframe
        self.dataframe = pd.read_csv(self.path)
        self.features = list(self.dataframe)
        self.price_title = 'SalePrice'
        self.non_numeric_title = ['Id', 'Neighborhood', 'LotConfig']
        self.price = self.dataframe[self.price_title]
        # Replace missing data
        self.mean = self.dataframe.mean()
        self.dataframe.fillna(self.mean, inplace=True)

    def plot_info(self, save_pdf=False, file_name=None, show_fig=True):
        filtered_dataframe = self.dataframe.drop(self.non_numeric_title + [self.price_title], axis=1, errors='ignore')
        filtered_features = list(filter(lambda f: f not in self.non_numeric_title + [self.price_title],
                                        self.features))
        fig = plt.figure(figsize=plt.figaspect(2/3)*2)
        for i, feature_name in enumerate(filtered_features):
            data = filtered_dataframe[feature_name]
            ax = fig.add_subplot(3, 3, i+1)
            ax.scatter(data, self.price, c=[np.random.rand(3,)])
            ax.set_xlabel(feature_name)
            ax.set_ylabel(self.price_title)

        fig.set_tight_layout(True)
        if show_fig:
            plt.show()
        if save_pdf:
            if file_name is None:
                file_name = 'a.pdf'
            out_path = path.join(path.dirname(__file__), "..", "results", file_name)
            fig.savefig(out_path)

    def fit_line(self, feature1, feature2):
        x = np.array(self.dataframe[feature1])
        y = np.array(self.dataframe[feature2])
        x_mean = x.mean()
        y_mean = y.mean()
        w = np.dot((x - x_mean), (y - y_mean)) / np.dot((x - x_mean), (x - x_mean))
        b = y_mean - w*x_mean
        return w, b

    def predict_price_linear(self, *features, save_pdf=False, show_fig=True):
        for feature_name in features:
            w, b = self.fit_line(feature_name, self.price_title)
            fig = plt.figure(figsize=plt.figaspect(2/3)*2)
            ax = fig.add_subplot(111)
            # scatter data
            data = self.dataframe[feature_name]
            price = self.dataframe[self.price_title]
            ax.scatter(data, price, c=[np.random.rand(3,)])
            # plot fit line
            predicted_price = w*data + b
            ax.plot(data, predicted_price, c=np.random.rand(3,))
            # calc rmse and print text
            rmse = np.sqrt(np.mean((predicted_price - price)**2))
            ax.text(data.min(), 350, 'w = {}\nb  = {}\nRMSE = {}'.format(round(w, 2), round(b, 2), round(rmse, 2)),
                    fontsize=26, family='serif')
            ax.set_xlabel(feature_name)
            ax.set_ylabel(self.price_title)
            # results
            if show_fig:
                plt.show()
            if save_pdf:
                out_path = path.join(path.dirname(__file__), "..", "results", "b-{}.pdf".format(feature_name))
                fig.savefig(out_path)

    def predict_price_knn(self, df, k=10, save_pdf=False, show_fig=True):
        filtered_features = self.dataframe.drop(self.non_numeric_title + [self.price_title], axis=1, errors='ignore')
        filtered_df = df.drop(self.non_numeric_title + [self.price_title], axis=1, errors='ignore')
        std_features = (filtered_features - filtered_features.min()) / \
                       (filtered_features.max() - filtered_features.min())
        std_df = (filtered_df - filtered_features.min()) / \
                 (filtered_features.max() - filtered_features.min())
        knn_index = (std_features - std_df.squeeze()).pow(2).sum(axis=1).pow(1/2).nsmallest(10).index.values.tolist()
        filtered_features = self.dataframe.drop(self.non_numeric_title, axis=1, errors='ignore')
        predicted_house = filtered_features.loc[knn_index, :].mean()
        return predicted_house[self.price_title]
