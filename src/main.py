from src.House import House
import pandas as pd


if __name__ == "__main__":
    house = House()
    # Part A
    house.plot_info(save_pdf=True, file_name='a.pdf')
    # Part B
    house.predict_price_linear('OverallQual', 'TotalBsmtSF', 'TotRmsAbvGrd', save_pdf=True)

    # Part E
    df_dict = {
             'Id': 13,
             'MSSubClass': 50,
             'LotArea': 7000,
             'LotConfig': 'Inside',
             'OverallQual': 4,
             'LotFrontage': 55,
             'Neighborhood': 'IDOTRR',
             'OverallCond': 5,
             'BedroomAbvGr': 2,
             'TotRmsAbvGrd': 5,
             'TotalBsmtSF': 586,
             'YearBuilt': 1923,
             'SalePrice': 55.5
    }
    df = pd.DataFrame(df_dict, index=[0])
    price = house.predict_price_knn(df)
    for i in range(house.dataframe.shape[0]):
        house.dataframe.loc[i, house.price_title] = house.predict_price_knn(pd.DataFrame(house.dataframe.loc[i, :]).T)
    house.plot_info(save_pdf=True, file_name='d.pdf')
    print("")
